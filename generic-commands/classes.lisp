(defpackage :generic-commands
  (:use :cl #+cmu :mop :clim)
  #+cmu (:shadowing-import-from :mop #:find-class #:built-in-class #:class-name #:class-of)
  (:shadowing-import-from :clim #:interactive-stream-p)
  (:export #:define-generic-command))

(in-package :generic-commands)

(defgeneric invoke-command (command-invocation))

(defclass generic-command (standard-class standard-method)
  ((ui-name :initarg :ui-name
            :reader command-ui-name)(command-precedence-list :reader command-precedence-list)
   (direct-options :initarg :direct-options
                   :reader command-direct-options)
   (direct-required-args :initarg :direct-required-args
                         :reader command-direct-required-args)
   (direct-optional-args :initarg :direct-optional-args
                         :reader command-direct-optional-args)
   (direct-keystroke :initarg :direct-keystroke
                     :reader command-direct-keystroke)
   (options :reader command-options)
   (required-args :reader command-required-args)
   (optional-args :reader command-optional-args)
   (keystroke :reader command-keystroke)))

(defclass abstract-command (generic-command)
  ())

(defclass concrete-command (generic-command)
  ((command-table :initarg :command-table
                  :reader command-command-table)
   (menu-spec :initarg :menu-spec
              :reader command-menu-spec)))

(defmethod (setf command-direct-keystroke) (new-value (command generic-command))
  (setf (slot-value command 'direct-keystroke))
  (keystroke-updated command))

(defmethod keystroke-updated ((command generic-command))
  (when (class-finalized-p command)
    (setf (slot-value command 'keystroke) (compute-keystroke command))
    (mapcar #'keystroke-updated (class-direct-subclasses command))))

(defmethod print-object ((object generic-command) stream)
  (print-unreadable-object (object stream :type nil :identity t)
    (write-string "Generic-Command " stream)
    (write (class-name object) :stream stream)))

(defmethod validate-superclass ((class generic-command) (super generic-command))
  t)

(defmethod validate-superclass ((class generic-command) (super t))
  (eql (class-name class) 'no-operation))

(defmethod reinitialize-instance ((instance generic-command)
                                  &rest initargs
                                  &key qualifiers lambda-list)
  ;; Since we are reinitializing a method, which should be forbidden, we
  ;; need to ensure that the critical identity-determining slots of the
  ;; method haven't changed. If that test passes, only then do we go
  ;; ahead and call shared-initialize.
  (cond
   ((not (equal (method-qualifiers instance) qualifiers))
    (error "Attempting to reinitialize a generic-command with a new qualifier."))
   ((or (not (consp lambda-list))
        (member (first lambda-list) '(&optional &key &rest))
        (not (endp (rest lambda-list))))
    (error "Attempting to reinitialize a generic-command with a lambda-list that does not match the expected pattern (a single required argument, nothing else)."))
   (t (apply #'shared-initialize instance nil initargs))))

(eval-when (:load-toplevel :execute)
  (ensure-class 'no-operation :metaclass 'abstract-command
                :direct-superclasses '(standard-object)
                :direct-slots '()
                :direct-options '()
                :direct-required-args '()
                :direct-optional-args '()
                :direct-keystroke nil
                :lambda-list '(.command.)
                :function (compile nil (lambda (&rest args)
                                         (declare (ignore args))
                                         (warn "Invoking No-Operation")))))

(defmethod compute-args ((command generic-command))
  ;; First get all the arg specs together in reverse precedence order
  (with-slots (required-args optional-args) command
    (let ((required-arg-specs (make-hash-table :test 'eq))
          (optional-arg-specs (make-hash-table :test 'eq)))
      (dolist (c (command-precedence-list command))
        ;; TODO: check that :when options are not in promoted args
        (dolist (spec (command-direct-required-args c))
          (let ((existing-optional-specs (gethash (car spec) optional-arg-specs)))
            (if existing-optional-specs
                ;; If it's already there as optional, promote it
                (progn
                  (setf (gethash (car spec) required-arg-specs) (cons (cdr spec) existing-optional-specs))
                  (remhash (car spec) optional-arg-specs))
              (push (cdr spec) (gethash (car spec) required-arg-specs)))))
        (dolist (spec (command-direct-optional-args c))
          (if (gethash (car spec) required-arg-specs)
              ;; If it's already there as required, respect that and promote this spec
              (push (cdr spec) (gethash (car spec) required-arg-specs))
            (push (cdr spec) (gethash (car spec) optional-arg-specs)))))
      ;; Combine the specs
      ;; TODO: Preserve the order of the args, since it probably changes the order that the prompts are shown
      (flet ((combine-arg-specs (hashtable)
               (let ((result nil))
                 (maphash (lambda (arg-name direct-specs)
                            (push (cons arg-name
                                        (let ((types (list 'and))
                                              (options '()))
                                          (dolist (spec direct-specs `(',types ,@options))
                                            (setf options (append (rest spec) options))
                                            ;; drop the quote around the type spec
                                            (push (second (first spec)) (rest types)))))
                                  result))
                          hashtable)
                 result)))
        (values (combine-arg-specs required-arg-specs)
                (combine-arg-specs optional-arg-specs))))))

(defun combine-keystroke-fragments (assembled-fragment new-fragment)
  (cond
   ((null assembled-fragment)
    new-fragment)
   ((null new-fragment)
    assembled-fragment)
   (t
    ;;; TODO!
    (cons new-fragment assembled-fragment))))

(defmethod compute-keystroke ((command generic-command))
  (reduce (lambda (new-fragment assembled-fragment)
            (combine-keystroke-fragments assembled-fragment new-fragment))
          (command-precedence-list command)
          :key 'command-direct-keystroke
          :initial-value nil
          :from-end t))

(defmethod (setf class-precedence-list) (value (command generic-command))
  (debug::break)
  (call-next-method))

(defmethod #-cmu finalize-inheritance #+cmu shared-initialize :after ((command generic-command) #+cmu slots #+cmu &key)
  (declare (ignore slots))
  (with-slots (command-precedence-list options keystroke required-args optional-args) command
    (setf command-precedence-list
          (remove-if-not (lambda (x) (typep x 'generic-command)) (class-precedence-list command)))
    (setf options (mapcan #'command-direct-options (command-precedence-list command)))
    (multiple-value-setq (required-args optional-args) (compute-args command))
    (setf keystroke (compute-keystroke command))))

(defmacro define-generic-command (name-and-options supercommands arguments
                                  &body body &environment env)
  ;; TODO: Qualifier?
  (declare (type list supercommands arguments))
  (destructuring-bind (name &rest options &key keystroke &allow-other-keys)
      (if (listp name-and-options)
          name-and-options
        (list name-and-options))
    (declare (type list options))
    (let* ((key-separator (position '&key arguments))
           (required-args (subseq arguments 0 key-separator))
           (optional-args (and key-separator (subseq arguments (1+ key-separator))))
           (arg-names `(,@(mapcar #'car required-args) ,@(mapcar #'car optional-args)))
           (options (copy-list options)))
      (remf options :keystroke)
      (multiple-value-bind (method-lambda extra-initargs)
          (make-method-lambda #'invoke-command (find-class 'no-operation)
                              `(lambda ,lambda-list
                                 (with-slots ,arg-names .command.
                                   ,@body))
                              env)
        `(ensure-class ',name :metaclass 'generic-command
                       :direct-superclasses ',(or supercommands '(no-operation))
                       :lambda-list '(.command.)
                       :function (compile nil ,method-lambda)
                       :direct-keystroke ',keystroke
                       :direct-options ',options
                       :direct-required-args ',required-args
                       :direct-optional-args ',optional-args
                       :direct-slots ',(mapcar (lambda (arg)
                                                `(:name ,arg :initargs (,arg)))
                                              arg-names)
                       ,@(mapcar (lambda (foo) `',foo) extra-initargs))))))

(defmethod initialize-instance :around ((command generic-command) &rest initargs)
  (multiple-value-prog1
   (apply #'call-next-method command :specializers (list command) initargs)
   (add-method #'invoke-command command)))

(defmethod compute-define-command-form ((command symbol))
  (compute-define-command-form (find-class command)))

(defmethod compute-define-command-form ((command generic-command))
  (flet ((spec-to-keyword-arg (spec)
           `(',(car spec) ,(car spec))))
    (let* ((required-args (command-required-args command))
           (optional-args (command-optional-args command)))
      `(define-command (,(class-name command)
                        :keystroke ,(command-keystroke command) ,@(command-options command))
         (,@required-args &key ,@optional-args)
         (invoke-command (make-instance ',(class-name command)
                                        ,@(mapcan #'spec-to-keyword-arg required-args)
                                        ,@(mapcan #'spec-to-keyword-arg optional-args)))))))

#| Sample code |

(define-generic-command (expression-movement :keystroke :alt) () ((distance 'integer) &key (interactive-p 'boolean :default nil))
  (list 'expression-movement distance interactive-p (call-next-method)))

(define-generic-command (move-up :keystroke (:ctrl #\u)) () (&key (structure 't :prompt "The structure") (distance 'integer :default 1))
  (list 'move-up structure distance (call-next-method)))

(define-generic-command (move-up-expression :documentation "Move up a nesting level") (move-up expression-movement) ((structure 'expression :prompt "No, me!"))
  (list 'move-up-expression structure (call-next-method)))

(define-concrete-command move-up-expression :command-table blah ... ?)

(compute-args (find-class 'move-up-expression))
(compute-define-command-form 'move-up-expression)
(macroexpand-1 (compute-define-command-form 'move-up-expression))
(mop:class-slots  (mop:find-class 'no-operation))
(invoke-command (make-instance 'move-up-expression 'distance 2 'structure 'structure 'interactive-p t))
(move-up-expression 2 'structure :interactive-p t)

||#
