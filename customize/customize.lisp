
(defmacro defcustom (var value doc &key (type 'expression) (setter 'set) (getter 'symbol-value))
  `(progn
     (defvar ,var ,value ,doc)
     (setf (get ',var 'customize-type) ,type)
     (setf (get ',var 'customize-setter) ,setter)
     (setf (get ',var 'customize-getter) ,getter)))

(defun customize (var &key (stream *query-io*) (view (stream-default-view stream)))
  (funcall (get var 'customize-setter)
           var
           (clim:accept (get var 'customize-type) stream view
                        :default (funcall (get var 'customize-getter) var))))
